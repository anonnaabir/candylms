<?php

class Db {
	private $main_DB;
	private $all_data;
	private $table;

	//Method for Database Connection
	public function database_connection($hostname,$database_user,$databass_pass,$database_name){
		$this->main_DB= new mysqli($hostname,$database_user,$databass_pass,$database_name);
		if ($this->main_DB->connect_errno>0) {
			die("Connection failed: " . $this->main_DB->connect_error);
		}

		 return $this->main_DB;		
	}

	//Method for Insert data to connected database table. CRUD = C
	public function database_insert($database_table,$database_column){
		$database_stroage="INSERT INTO $database_table SET $database_column";
		$this->all_data=$this->main_DB->query($database_stroage);
		return $this->all_data;
	}

	//Method for Read data from connected database table. CRUD = R
	public function database_read($database_table){
		$database_stroage="SELECT * FROM $database_table";
		$this->all_data=$this->main_DB->query($database_stroage)->fetch_all(MYSQLI_ASSOC);


		return $this->all_data;	
	}


	//Method for Edit data from the connected database table. CRUD = U
	public function database_edit(){

	}

	//Method for Delete data from connected database table. CRUD = D
	public function database_delete($database_table,$id_number){
		$database_stroage="DELETE FROM $database_table WHERE Number=$id_number";
		$this->all_data=$this->main_DB->query($database_stroage);
		return $database_stroage;
	}

}

//$all_reports = new Db;
//$all_reports->database_connection("localhost","root","","arlms");
//$all_reports->database_delete("books",37);
//echo  $all_reports->report_table("books");
?>

