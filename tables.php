<?php
require "db.php";
require "header.php";
require "sidebar.php";
require "modal.php";
require "delete.php";

/**
 * 
 */
class Table extends Db
{
  
  public function report_table($table_name){
    $all_reports = new Db;
    $all_reports->database_connection("localhost","root","","arlms");
    $final_value = $all_reports->database_read("$table_name");
    $table="<table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">";
    $table.="<thead>";
    $table.="<tr>";
    foreach ($final_value[0] as $all_column => $all_row) {
          $table.="<th>$all_column</th>";
        }

        $table.="<th>Actions</th>";
        $table.="</tr>";
        $table.="</thead>";
        $table.="<tbody>";
    foreach ($final_value as $all_row) {
      $table.="<tr>";
        foreach ($all_row as $value) {
          echo "<pre>";
          print_r($_REQUEST);
          echo "</pre>";
          $table.="<td class=\"column1\">$value</td>";
        }
      $table.="<td><a href=\"delete?id?$all_row[Number]\" type=\"button\" class=\"btn btn-danger btn-circle btn-sm\" data-toggle=\"modal\" data-target=\"#deleteModal\"><i class=\"fas fa-trash\"></i></a>&nbsp&nbsp<a href=\"edit?id=$all_row[Number]\" class=\"btn btn-info btn-circle btn-sm\"><i class=\"far fa-edit\"></i></a></td>";
      $table.="</tr>";

    }
    $table.="</tbody>"; 
    $table.="</table>";
    return $table;

  }

  public function delete_data(){

  }
  
}

?>


<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tables</h1>
<p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
<div class="card-header py-3">
<h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
</div>
<div class="card-body">
                  
<div class="table-responsive">
<?php
$report= new Table;
echo $report->report_table("books");?>
</div>
</div>
</div>
</div>



        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

</body>

</html>
